<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/create-pengajuan', [App\Http\Controllers\CreatePengajuanController::class, 'index'])->middleware(['checkRole:admin,atasan,pekerja']);
Route::get('cities/get_by_country', 'CitiesController@get_by_country')->name('admin.cities.get_by_country');
Route::get('pengajuan/get_by_kategori/{kategori_id}', [App\Http\Controllers\CreatePengajuanController::class, 'get_by_kategori']);
Route::post('pengajuan/store', [App\Http\Controllers\CreatePengajuanController::class, 'store']);
Route::get('pengajuan/jumlah', [App\Http\Controllers\CreatePengajuanController::class, 'jumlah']);

//TRACK DAN APPROVE PENGAJUAN
Route::get('/trackpengajuan', [App\Http\Controllers\TrackPengajuanController::class, 'index']);
Route::get('/approve', [App\Http\Controllers\TrackPengajuanController::class, 'indexapproveadmin']);
Route::get('/approveatasan/{id}', [App\Http\Controllers\TrackPengajuanController::class, 'approvedatasan']);
Route::post('/rejectatasan/{id}', [App\Http\Controllers\TrackPengajuanController::class, 'rejectedatasan']);
//APPROVE ADMIN
Route::get('/approveadmin/{id}', [App\Http\Controllers\TrackPengajuanController::class, 'approvedadmin']);
Route::post('/rejectadmin/{id}', [App\Http\Controllers\TrackPengajuanController::class, 'rejectedadmin']);

//Pemusnahan APD
Route::get('/pemusnahanapd', [App\Http\Controllers\PemusnahanController::class, 'index']);
Route::get('/addpemusnahan', [App\Http\Controllers\PemusnahanController::class, 'indexadd']);
Route::post('pemusnahan/store', [App\Http\Controllers\PemusnahanController::class, 'store']);


//Gudang APD
Route::get('/kategoriapd', [App\Http\Controllers\GudangContoller::class, 'indexkategori']);
Route::get('/addkategori', [App\Http\Controllers\GudangContoller::class, 'indexaddkategori']);
Route::post('/actionaddkategori', [App\Http\Controllers\GudangContoller::class, 'actionaddkategori']);
Route::get('/indexapd', [App\Http\Controllers\GudangContoller::class, 'indexapd']);
Route::get('/addapd', [App\Http\Controllers\GudangContoller::class, 'indexaddapd']);
Route::post('/actionaddapd', [App\Http\Controllers\GudangContoller::class, 'actionaddapd']);
Route::get('/stockapd', [App\Http\Controllers\GudangContoller::class, 'indexstockapd']);
Route::get('/pesanapd/{id}', [App\Http\Controllers\GudangContoller::class, 'pesanstockapd']);
Route::post('/actionaddstock/{id}', [App\Http\Controllers\GudangContoller::class, 'actionaddstock']);

//Abot 
Route::get('/indexsop', [App\Http\Controllers\SopController::class, 'index']);
Route::get('/editsop/{id}', [App\Http\Controllers\SopController::class, 'edit']);
Route::get('/addsop', [App\Http\Controllers\SopController::class, 'IndexCreate']);
Route::post('/sop/edit/{id}', [App\Http\Controllers\SopController::class, 'Actionedit']);
Route::get('/sop/delete/{id}', [App\Http\Controllers\SopController::class, 'Actiondelete']);
Route::post('/sop/add', [App\Http\Controllers\SopController::class, 'Actionadd']);
Route::post('/edit/visi', [App\Http\Controllers\VisimisiController::class, 'Actioneditvisi']);
Route::get('/indexvisi', [App\Http\Controllers\VisimisiController::class, 'indexvisi']);
Route::get('/indexmisi', [App\Http\Controllers\VisimisiController::class, 'indexmisi']);
Route::post('/edit/misi', [App\Http\Controllers\VisimisiController::class, 'Actioneditmisi']);

//Laporan Pelanggaran
Route::get('/indexlaporan', [App\Http\Controllers\LaporanController::class, 'index']);
Route::get('/pelanggaransaya', [App\Http\Controllers\LaporanController::class, 'indexmine']);
Route::get('/addlaporan', [App\Http\Controllers\LaporanController::class, 'IndexCreate']);
Route::post('/laporan/add', [App\Http\Controllers\LaporanController::class, 'Actionadd']);


//Cetak
Route::get('/cetakbukti/{id}', [App\Http\Controllers\PrintController::class, 'cetakbukti']);
Route::get('/cetakbuktilaporan/{id}', [App\Http\Controllers\PrintController::class, 'cetakbuktilaporan']);
Route::get('/cetakbuktipemusnahan/{id}', [App\Http\Controllers\PrintController::class, 'cetakbuktipemusnahan']);

//Excels
Route::get('/export-excels-riwayat', [App\Http\Controllers\PrintController::class, 'excelsriwayat']);
Route::get('/export-excels-laporan', [App\Http\Controllers\PrintController::class, 'excelslaporan']);
Route::get('/export-excels-pemusnahan', [App\Http\Controllers\PrintController::class, 'excelspemusnahan']);


//Kebutuhan APD
Route::get('/indexplant1', [App\Http\Controllers\Plant1Controller::class, 'index']);
Route::get('/editplant1/{id}', [App\Http\Controllers\Plant1Controller::class, 'edit']);
Route::get('/addplant1', [App\Http\Controllers\Plant1Controller::class, 'IndexCreate']);
Route::post('/plant1/edit/{id}', [App\Http\Controllers\Plant1Controller::class, 'Actionedit']);
Route::get('/plant1/delete/{id}', [App\Http\Controllers\Plant1Controller::class, 'Actiondelete']);
Route::post('/plant1/add', [App\Http\Controllers\Plant1Controller::class, 'Actionadd']);
