<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Profile extends Model
{
    protected $table = 'profil';

    protected $fillable = ['name', 'no_reg','jabatan', 'user_id'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User);
    }
}
