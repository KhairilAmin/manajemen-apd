<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriApd extends Model
{
    protected $table = 'kategori_apd';

    protected $fillable = ['kategori'];
    public $timestamps = false;
    use HasFactory;
    use HasFactory;
}
