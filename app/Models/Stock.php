<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';

    protected $fillable = ['id_apd', 'stock_min','stock'];
    public $timestamps = false;
    
    use HasFactory;
}
