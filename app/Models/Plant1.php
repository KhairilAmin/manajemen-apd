<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plant1 extends Model
{
    protected $table = 'plant1';

    protected $fillable = ['job', 'apd', 'departemen'];
    public $timestamps = false;
    use HasFactory;
}
