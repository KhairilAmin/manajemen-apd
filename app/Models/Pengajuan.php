<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    protected $table = 'pengajuan';

    protected $fillable = ['id_pengajuan_detail', 'nomor_pengajuan','id_pengaju', 'approved','id_atasan','approved_by_admin','id_admin','tanggal_pengajuan','tanggal_approve','alasan'];
    public $timestamps = false;
    
    public function user()
    {
        return $this->belongsTo(User);
    }
}
