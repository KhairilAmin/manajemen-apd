<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemusnahan extends Model
{
    protected $table = 'pemusnahan';

    protected $fillable = ['id_pemusnah','jumlah_apd', 'id_apd','bukti_foto','created_at','updated_at'];
    public $timestamps = false;

    public function apd()
    {
        return $this->belongsTo(Apd::class, 'id_apd');
    }

    public function pemusnah()
    {
        return $this->belongsTo(User::class, 'id_pemusnah');
    }
}
