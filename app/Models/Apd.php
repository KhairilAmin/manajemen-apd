<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apd extends Model
{
    protected $table = 'apd';

    protected $fillable = ['nama', 'id_kategori','has_exp','jangka_waktu'];
    public $timestamps = false;
    use HasFactory;

    public function riwayat()
    {
        return $this->hasMany(Apd::class, 'id');
    }

    public function pemusnahan()
    {
        return $this->hasMany(Apd::class, 'id');
    }
}
