<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sop extends Model
{
    protected $table = 'sop';

    protected $fillable = ['isi_sop'];
    public $timestamps = false;
    
    use HasFactory;
}
