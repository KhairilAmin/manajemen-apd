<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    protected $table = 'laporan';

    protected $fillable = ['id_pelapor','id_terlapor','pelanggaran','tindakan'];

    public $timestamps = false;
    
    use HasFactory;
    
    public function terlapor()
    {
        return $this->belongsTo(User::class, 'id_terlapor');
    }
    public function pelapor()
    {
        return $this->belongsTo(User::class, 'id_pelapor');
    }
}
