<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Riwayat extends Model
{
    protected $table = 'riwayat';

    protected $fillable = ['id_apd', 'action','jumlah_apd','created_at'];
    public $timestamps = false;

    public function apd()
    {
        return $this->belongsTo(Apd::class, 'id_apd');
    }
}

