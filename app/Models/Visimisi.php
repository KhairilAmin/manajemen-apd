<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visimisi extends Model
{
    protected $table = 'visimisi';

    protected $fillable = ['type','isi'];
    public $timestamps = false;
    
    use HasFactory;
}
