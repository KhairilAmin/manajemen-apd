<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanDetail extends Model
{
    protected $table = 'pengajuan_detail';

    protected $fillable = ['id_apd', 'id_pengajuan','jumlah_apd','exp_date'];
    public $timestamps = false;
}
