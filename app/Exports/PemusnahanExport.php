<?php

namespace App\Exports;

use App\Models\Pemusnahan;
use App\Models\Riwayat;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PemusnahanExport implements FromView
{
    public function view(): View
    {
        return view('pemusnahanexcel', [
            'datas' => Pemusnahan::all()
        ]);
    }
}
