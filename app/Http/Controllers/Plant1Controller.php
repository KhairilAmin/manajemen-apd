<?php

namespace App\Http\Controllers;

use App\Models\Plant1;
use Illuminate\Http\Request;

class Plant1Controller extends Controller
{
    public function index()
    {
        $plant1 = Plant1::get();
        return view('kebutuhan_apd/plant1/datalist', compact('plant1'));
    }
    public function edit($id)
    {
        $plant1 = Plant1::where('id', $id)->first();
        return view('kebutuhan_apd/plant1/edit', compact('plant1'));
    }
    public function Actionedit(Request $request, $id)
    {
        $plant1 = Plant1::where('id', $id)->first();
        $plant1->job = $request->job;
        $plant1->apd = $request->apd;
        $plant1->save();

        return redirect('/indexplant1');
    }
    public function IndexCreate()
    {
        return view('kebutuhan_apd/plant1/add');
    }
    public function Actionadd(Request $request)
    {
        $plant1 = Plant1::create([
            'job' => $request->job,
            'apd' => $request->apd,
        ]);

        return redirect('/indexplant1');
    }
    public function Actiondelete(Request $request, $id)
    {
        $plant1 = Plant1::where('id', $id)->delete();

        return redirect('/indexplant1');
    }
}
