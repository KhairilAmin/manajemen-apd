<?php

namespace App\Http\Controllers;

use App\Models\Apd;
use App\Models\Pemusnahan;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PemusnahanController extends Controller
{
    public function index(){
        $pemusnahan = Pemusnahan::get();
        return view('pemusnahan/index', compact('pemusnahan'));
    }
    public function indexadd(){
        $pemusnahan = Pemusnahan::get();
        $apd = Apd::get();

        return view('pemusnahan/indexadd', compact('pemusnahan','apd'));
    }
    public function store(Request $request){
        $user=Auth::user();
        $current=Carbon::now();
        $image = $request->file('bukti');
        // dd($request);
        $image->storeAs('public/photos', $image->hashName());

        Pemusnahan::create([
            'id_pemusnah' => $user->id,
            'id_apd' => $request->apd,
            'jumlah_apd'=>$request->jumlah_apd,
            'bukti_foto' => $image->hashName(),
            'created_at' => $current,
        ]);


        return redirect('/pemusnahanapd');

    }
}
