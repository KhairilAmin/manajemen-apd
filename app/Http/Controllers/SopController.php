<?php

namespace App\Http\Controllers;

use App\Models\Sop;
use Illuminate\Http\Request;

class SopController extends Controller
{
    public function index(){
        $sop = Sop::get();
        return view('tentang/sop/datalist', compact('sop'));
    }
    public function edit($id){
        $sop = Sop::where('id',$id)->first();
        return view('tentang/sop/edit',compact('sop'));
    }
    public function Actionedit(Request $request, $id){
        $sop = Sop::where('id',$id)->first();
        $sop->isi_sop= $request->sop;
        $sop->save();

        return redirect('/indexsop');

    }
    public function IndexCreate(){
        return view('tentang/sop/add');
    }
    public function Actionadd(Request $request){
        $sop=Sop::create([
            'isi_sop' => $request->sop,            
        ]);

        return redirect('/indexsop');

    }
    public function Actiondelete(Request $request, $id){
        $sop = Sop::where('id',$id)->delete();

        return redirect('/indexsop');
    }

}
