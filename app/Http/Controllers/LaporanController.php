<?php

namespace App\Http\Controllers;

use App\Models\Laporan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index(){
        $auth=Auth::user();
        $data=[];
        $key=0;
        // $tes= Laporan::get();
        $laporan=Laporan::get();
        foreach ($laporan as $laporans){
            $pelapor = User::where('id',$laporans->id_pelapor)->first();
            $terlapor = User::where('id',$laporans->id_terlapor)->first();
            $data[$key]['id']=$laporans->id;
            $data[$key]['pelapor']=$pelapor->name;
            $data[$key]['terlapor']=$terlapor->name;
            $data[$key]['pelanggaran']=$laporans->pelanggaran;
            $data[$key]['tindakan']=$laporans->tindakan;
            $key++;
        }
        // dd($data);
        return view('laporan/datalist',compact('data','auth')); 
    }
    public function indexmine(){
        $auth=Auth::user();
        $data=[];
        $key=0;
        $laporan=Laporan::where('id_terlapor',auth()->user()->id)->orderBy('id', 'DESC')->get();
        foreach ($laporan as $laporans){
            $pelapor = User::where('id',$laporans->id_pelapor)->first();
            $terlapor = User::where('id',$laporans->id_terlapor)->first();
            $data[$key]['id']=$laporans->id;
            $data[$key]['pelapor']=$pelapor->name;
            $data[$key]['terlapor']=$terlapor->name;
            $data[$key]['pelanggaran']=$laporans->pelanggaran;
            $data[$key]['tindakan']=$laporans->tindakan;
            $key++;
        }
        // dd($laporan);
        return view('laporan/datalist',compact('data','auth')); 
    }
    public function IndexCreate(){
        $user = User::get();
        return view('laporan/add',compact('user'));
    }
    public function Actionadd(Request $request){
        $user=Auth::user();
        $pelaporan=Laporan::create([
            'id_pelapor'=>$user->id,
            'id_terlapor' =>$request->terlapor,
            'pelanggaran' => $request->pelanggaran,
            'tindakan' => $request->tindakan,          
        ]);

        return redirect('/indexlaporan');

    }
}
