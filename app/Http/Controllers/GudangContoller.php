<?php

namespace App\Http\Controllers;

use App\Models\Apd;
use App\Models\Riwayat;
use App\Models\KategoriApd;
use App\Models\Stock;
use Illuminate\Http\Request;
use Carbon\Carbon;


class GudangContoller extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    public function indexkategori(){
        $kategori = KategoriApd::get();

        return view('apd/kategori/datalist', compact('kategori'));
    }
    
    public function indexaddkategori(){
        $kategori = KategoriApd::get();

        return view('apd/kategori/add', compact('kategori'));
    }

    public function actionaddkategori(Request $request){
        KategoriApd::create([
            'kategori' => $request->kategori
        ]);
        return redirect('/kategoriapd');
    }

    public function indexapd(){
        $apd = Apd::get();
        $key = 0;
        $data=[];
        foreach ($apd as $apd){
            $kategori=KategoriApd::where('id',$apd->id_kategori)->first();
            $data[$key]['apd']=$apd->nama;
            $data[$key]['kategori']=$kategori->kategori;
            $key++;
        }
        // dd($data);
        return view('apd/apd/datalist', compact('data'));
    }

    public function indexaddapd(){
        $kategori = KategoriApd::get();

        return view('apd/apd/add', compact('kategori'));
    }

    public function actionaddapd(Request $request){
        $apd=Apd::create([
            'id_kategori' => $request->kategori,
            'nama' => $request->apd,
            'has_exp' => $request->has_exp,
            'jangka_waktu' => $request->jangkawaktu,
            
        ]);
        Stock::create([
            'id_apd'=> $apd->id,
            'stock_min' =>$request->stock_min,
            'stock' => 0
        ]);
        return redirect('/indexapd');
    }

    public function indexstockapd(){
        $stok = Stock::get();
        $kategori = KategoriApd::get();
        $riwayat = Riwayat::orderBy('id', 'DESC')->get();
        $data = [];
        $key = 0;
        foreach($stok as $stock){
            $apd = Apd::where('id',$stock->id_apd)->first();
            $data[$key]['apd'] = $apd->nama;
            $data[$key]['id_apd'] = $apd->id;
            $data[$key]['stock'] = $stock->stock; 
            $data[$key]['stock_min'] = $stock->stock_min; 
            $key++;
        }
        // dd($data);
        return view('apd/stock/datalist',compact('data','riwayat'));
    }

    public function pesanstockapd(Request $request,$id){
        $apd = Apd::where('id',$id)->first();

        return view('apd/stock/tambahstock',compact('apd'));
    }
    public function actionaddstock(Request $request,$id){
        $current = Carbon::today()->toDateString();
        // dd($current);
        $tes = Stock::where('id_apd',$id)->first();
        Riwayat::create([
            'id_apd' =>$tes->id_apd,
            'action' => 'Penambahan',
            'jumlah_apd' => $request->jumlah_apd,
            'created_at' => $current,
        ]);
        // dd($tes);
        $tes->stock = $tes->stock + $request->jumlah_apd;
        $tes->save(); 
        return redirect('/stockapd');
    }
} 
