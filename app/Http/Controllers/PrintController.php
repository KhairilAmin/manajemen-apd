<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Pengajuan;
use App\Models\Apd;
use App\Models\Laporan;
use App\Models\Pemusnahan;
use App\Models\User;
use PDF;
use Excel;
use App\Exports\RiwayatExport;
use App\Exports\LaporanExport;
use App\Exports\PemusnahanExport;

use Illuminate\Http\Request;

class PrintController extends Controller
{
    public function cetakbukti($id)
    {
        $auth = Auth::user();
        $data = [];
        $key = 0;
        $pengajuan = Pengajuan::where('id', $id)->get();
        foreach ($pengajuan as $pengajuan) {
            $apd = Apd::where('id', $pengajuan->id_apd)->first();
            $users = User::where('id', $pengajuan->id_pengaju)->first();
            $admin = User::where('id', $pengajuan->id_admin)->first();
            $atasan = User::where('id', $pengajuan->id_atasan)->first();
            $data[$key]['id'] = $pengajuan->id;
            $data[$key]['tanggal_pengajuan'] = $pengajuan->tanggal_pengajuan;
            $data[$key]['tanggal_approve'] = $pengajuan->tanggal_approve;
            $data[$key]['nomor_pengajuan'] = $pengajuan->nomor_pengajuan;
            // $data[$key]['exp_date']=$pengajuan->exp_date;
            // $data[$key]['apd']=$apd->nama;
            // $data[$key]['jumlah_apd']=$pengajuan->jumlah_apd;
            $data[$key]['pengaju'] = $users->name;
            $data[$key]['admin'] = $admin->name;
            $data[$key]['atasan'] = $atasan->name;
            $key++;
        }
        $pdf = PDF::loadview('cetakbukti_pdf', ['data' => $data]);
        return $pdf->download('cetakbukti-pdf.pdf');
    }

    public function cetakbuktilaporan($id)
    {
        $auth = Auth::user();
        $data = [];
        $key = 0;
        $laporan = Laporan::where('id', $id)->get();
        foreach ($laporan as $laporans) {
            $usersterlapor = User::where('id', $laporans->id_terlapor)->first();
            $userspelapor = User::where('id', $laporans->id_pelapor)->first();
            $data[$key]['id'] = $laporans->id;
            $data[$key]['pelanggaran'] = $laporans->pelanggaran;
            $data[$key]['tindakan'] = $laporans->tindakan;
            $data[$key]['terlapor'] = $usersterlapor->name;
            $data[$key]['pelapor'] = $userspelapor->name;
            $key++;
        }
        $pdf = PDF::loadview('cetakbuktilapor_pdf', ['data' => $data]);
        return $pdf->download('cetakbuktilapor_pdf.pdf');
    }

    public function cetakbuktipemusnahan($id)
    {
        $auth = Auth::user();
        $data = [];
        $key = 0;
        $pemusnahan = Pemusnahan::where('id', $id)->get();
        foreach ($pemusnahan as $pemusnahans) {
            $userspemusnah = User::where('id', $pemusnahans->id_pemusnah)->first();
            $apd = Apd::where('id', $pemusnahans->id_apd)->first();
            $data[$key]['id'] = $pemusnahans->id;
            $data[$key]['id_pemusnah'] = $userspemusnah->name;
            $data[$key]['id_apd'] = $apd->nama;
            $data[$key]['jumlah_apd'] = $pemusnahans->jumlah_apd;
            $data[$key]['bukti_foto'] = $pemusnahans->bukti_foto;
            $key++;
        }

        $pdf = PDF::loadview('cetakbuktipemusnahan_pdf', ['data' => $data]);
        return $pdf->download('cetakbuktipemusnahan_pdf.pdf');
    }

    public function excelsriwayat(){
        $nama_file = 'riwayat_apd' . date('Y-m-d_H_i_s') . '.xlsx';
        return Excel::download(new RiwayatExport, $nama_file);
    }
    
    public function excelslaporan(){
        $nama_file = 'laporan_apd' . date('Y-m-d_H_i_s') . '.xlsx';
        return Excel::download(new LaporanExport, $nama_file);
    }

    public function excelspemusnahan(){
        $nama_file = 'pemusnahan_apd' . date('Y-m-d_H_i_s') . '.xlsx';
        return Excel::download(new PemusnahanExport, $nama_file);
    }
}
