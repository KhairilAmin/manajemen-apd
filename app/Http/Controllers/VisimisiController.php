<?php

namespace App\Http\Controllers;

use App\Models\Visimisi;
use Illuminate\Http\Request;

class VisimisiController extends Controller
{
    public function indexvisi(){
        $visi = Visimisi::where('type','visi')->first();
        return view('tentang/visi/edit', compact('visi'));
    }
    public function Actioneditvisi(Request $request){
        $visi = Visimisi::where('type','visi')->first();
        $visi->isi= $request->visi;
        $visi->save();

        return redirect('/indexvisi');

    }
    public function indexmisi(){
        $misi = Visimisi::where('type','misi')->first();
        return view('tentang/misi/edit', compact('misi'));
    }
    public function Actioneditmisi(Request $request){
        $misi = Visimisi::where('type','misi')->first();
        $misi->isi= $request->misi;
        $misi->save();

        return redirect('/indexmisi');

    }
}
