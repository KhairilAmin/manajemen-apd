<?php

namespace App\Http\Controllers;

use App\Models\Pengajuan;
use App\Models\User;
use App\Models\Apd;
use App\Models\KategoriApd;
use App\Models\PengajuanDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class CreatePengajuanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    public function jumlah()
    {
        return view('pengajuan/create/jumlah');
    }
    public function index(Request $request)
    {
        $loop = $request->jumlah;
        $loop1 = $loop + 1;
        $user = Auth::user();
        $apd = Apd::get();
        $kategori_apd = KategoriApd::get();
        return view('pengajuan/create/datalist', compact('user', 'apd', 'kategori_apd', 'loop1'));
    }
    public function get_by_kategori(Request $request)
    {

        if (!$request->kategori_id) {
            $html = '<option value="">' . trans('global.pleaseSelect') . '</option>';
        } else {
            $html = '';
            $apd = Apd::where('id_kategori', $request->kategori_id)->get();
            foreach ($apd as $apd) {
                $html .= '<option value="' . $apd->id . '">' . $apd->nama . '</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $current = Carbon::now();
        $jumlah = Pengajuan::get()->count();

        $pengajuan = Pengajuan::create([
            'id_pengajuan_detail' => 1,
            'nomor_pengajuan' => 'FRM/R-APD/' . $current->format('dmy') . '/' . $jumlah,
            'id_pengaju' => $user->id,
            'approved' => 0,
            'id_atasan' => null,
            'approved_by_admin' => 0,
            'id_admin' => null,
            'tanggal_pengajuan' => $current

        ]);

        foreach ($request->input('apd') as $key => $value) {
            // UPLOAD IMAGE


            PengajuanDetail::create([
                'id_pengajuan' => $pengajuan->id,
                'id_apd' => $request->apd[$key],
                'jumlah_apd' => $request->jumlah_apd[$key]
            ]);
        }

        return redirect('/trackpengajuan');
    }
}
