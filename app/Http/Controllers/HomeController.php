<?php

namespace App\Http\Controllers;

use App\Models\Apd;
use App\Models\Stock;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $stok=Stock::get();
        $key=0;
        $data=[];
            foreach($stok as $stock){
                $apd = Apd::where('id',$stock->id_apd)->first();
                $data[$key]['apd'] = $apd->nama;
                $data[$key]['id_apd'] = $apd->id;
                $data[$key]['stock'] = $stock->stock; 
                $data[$key]['stock_min'] = $stock->stock_min; 
                $key++;
            }
            return view('dashboard',compact('data'));
    }
}
