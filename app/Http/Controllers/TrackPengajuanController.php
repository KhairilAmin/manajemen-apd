<?php

namespace App\Http\Controllers;
use Illuminate\Support\Carbon;
use App\Models\Apd;
use App\Models\Pengajuan;
use App\Models\PengajuanDetail;
use App\Models\Riwayat;
use App\Models\Stock;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TrackPengajuanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $auth=Auth::user();
        $data=[];
        $key=0;
        $pengajuan=Pengajuan::where('id_pengaju',auth()->user()->id)->orderBy('id', 'DESC')->get();
        foreach ($pengajuan as $pengajuan){
            $apd=Apd::where('id',$pengajuan->id_apd)->first();
            $users=User::where('id',$pengajuan->id_pengaju)->first();
            $data[$key]['id']=$pengajuan->id;
            $data[$key]['alasan']=$pengajuan->alasan;
            $data[$key]['nomor_pengajuan']=$pengajuan->nomor_pengajuan;
            $data[$key]['pengaju']=$users->name;
            $data[$key]['approved']=$pengajuan->approved;
            $data[$key]['approved_by_admin']=$pengajuan->approved_by_admin;
            $key++;
        }
        return view('pengajuan/track/datalist',compact('data','auth')); 
    }

    public function indexapproveadmin(){
        $auth=Auth::user();
        $data=[];
        $key=0;
        $pengajuan=Pengajuan::orderBy('id', 'DESC')->get();
        foreach ($pengajuan as $pengajuan){
            $apd=Apd::where('id',$pengajuan->id_apd)->first();
            $users=User::where('id',$pengajuan->id_pengaju)->first();
            $data[$key]['id']=$pengajuan->id;
            $data[$key]['nomor_pengajuan']=$pengajuan->nomor_pengajuan;
            $data[$key]['pengaju']=$users->name;
            $data[$key]['approved']=$pengajuan->approved;
            $data[$key]['approved_by_admin']=$pengajuan->approved_by_admin;
            $key++;
        }
        return view('pengajuan/approve/datalist',compact('data','auth')); 
    }
    
    public function approvedatasan(Request $request,$id)
    {
        $auth=Auth::user();
        $pengajuan=Pengajuan::find($id);
        $pengajuan->approved=1;
        $pengajuan->id_atasan=$auth->id;
        $pengajuan->save();
        return redirect('/trackpengajuan');

    }
    public function rejectedatasan(Request $request,$id)
    {
        $auth=Auth::user();
        $pengajuan=Pengajuan::find($id);
        $pengajuan->approved=10;
        $pengajuan->alasan = $request->alasan;
        $pengajuan->id_atasan=$auth->id;
        $pengajuan->save();
        return redirect('/trackpengajuan');

    }
    public function approvedadmin(Request $request,$id)
    {
        $current2 = Carbon::today()->toDateString();
        $current=Carbon::today();
        $tanggal_approve=Carbon::now();
        $auth=Auth::user();
        $pengajuan=Pengajuan::find($id);
        $pengajuan_detail = PengajuanDetail::where('id_pengajuan',$pengajuan->id)->get();

        foreach($pengajuan_detail as $pengajuan_detail)
        {
            $apd=Apd::find($pengajuan_detail->id_apd);
            $stock=Stock::where('id_apd',$pengajuan_detail->id_apd)->first();
            $exp=$current->addDays($apd->jangka_waktu);
            
            if($apd->has_exp==1){
                $pengajuan_detail->exp_date=$exp;
                // dd($pengajuan_detail->exp_date);
                $pengajuan_detail->save();
            }
            $stock->stock= $stock->stock - $pengajuan_detail->jumlah_apd;
            $stock->save();

            Riwayat::create([
                'id_apd' =>$pengajuan_detail->id_apd,
                'action' => 'Pengurangan',
                'jumlah_apd' => $pengajuan_detail->jumlah_apd,
                'created_at' => $current2,
            ]);

        }
        
        $pengajuan->approved_by_admin=1;
        $pengajuan->id_admin=$auth->id;
        $pengajuan->tanggal_approve=$tanggal_approve;
        $pengajuan->save();

        
        return redirect('/trackpengajuan');

    }
    public function rejectedadmin(Request $request,$id)
    {
        $auth=Auth::user();
        $pengajuan=Pengajuan::find($id);
        $pengajuan->approved_by_admin=10;
        $pengajuan->alasan = $request->alasan;
        $pengajuan->id_admin=$auth->id;
        $pengajuan->save();
        return redirect('/trackpengajuan');

    }
}
