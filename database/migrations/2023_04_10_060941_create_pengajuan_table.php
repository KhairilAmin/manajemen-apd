<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('id_pengajuan_detail');
            $table->string('nomor_pengajuan');
            $table->integer('id_pengaju');
            $table->integer('approved')->default(0);
            $table->integer('id_atasan')->nullable();
            $table->integer('approved_by_admin')->default(0);
            $table->integer('id_admin')->nullable();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan');
    }
}
