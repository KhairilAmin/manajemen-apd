<!DOCTYPE html>
<html lang="en">
<style>
    .header h1{
        text-align: center;
    }
</style>
<body>
    <div class="header">
        <h1>Bukti Pengajuan APD</h1>
    </div>
    <hr>
    @forelse ($data as $key => $item)    
    <div>
        <p>
            Nama Pengaju &emsp;&emsp;&emsp;: {{$item['pengaju']}}
        <br>
            
        @php
            $detail = App\Models\PengajuanDetail::where('id_pengajuan',$item['id'])->get();
            $data1=[];
            foreach($detail as $key => $item1) {
                $apd = App\Models\Apd::where('id', $item1->id_apd)->first();
                $data1[$key]['apd'] = $apd->nama;
                $data1[$key]['exp_date'] = $item1->exp_date;
                $data1[$key]['jumlah'] = $item1->jumlah_apd;
            }
                                
        @endphp
            APD yang diajukan &emsp;:
        @foreach ( $data1 as $item1)
            <br>{{$item1['apd']}} , Jumlah : {{$item1['jumlah']}}, Tanggal Expired : @if(is_null($item1['exp_date'])) - @else {{$item1['exp_date']}} @endif
        @endforeach
		<br>
			Tanggal Pengajuan&emsp;&ensp;: {{$item['tanggal_pengajuan']}}
		<br>
			Nomor Pengajuan Pengajuan&emsp;&ensp;: {{$item['nomor_pengajuan']}}
		<br>
		------------------------------------------------------------------------------------
		<br>
            Pemberi Approve 
		<br>
		<br>
            Nama Atasan&emsp;&emsp;&emsp;&ensp;&ensp;: {{$item['atasan']}}
		<br>
            Nama Admin&emsp;&emsp;&emsp;&emsp;: {{$item['admin']}}
		<br>
            Tanggal Approve&emsp;&emsp;&ensp;: {{$item['tanggal_approve']}}
		<br>
    </div>
    @empty
        
    @endforelse
</body>
</html>