@extends('template')
@section('content')
    <div class="row">
        <a href="/addpemusnahan" type="button" class="btn btn-success mb-2">Buat Pemusnahan</a>
        <a href="/export-excels-pemusnahan" type="button" class="btn btn-danger mb-2" style="margin-left:1rem">Cetak Excel</a>
        <table class="table table-bordered user_datatable">
            <thead>
                <th>NO</th>
                <th>Pemusnah</th>
                <th>APD</th>
                <th>Jumlah</th>
                <th>bukti</th>
                <th>Cetak Bukti</th>
            </thead>
            <tbody>
                @foreach ($pemusnahan as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    @php
                        $orang = App\Models\User::where('id',$item->id_pemusnah)->first();
                        $apd = App\Models\Apd::where('id',$item->id_apd)->first();
                    @endphp
                    <td>{{$orang->name}}</td>
                    <td>{{$apd->nama}}</td>
                    <td>{{$item->jumlah_apd}}</td>
                    <td>
                        <img src="{{url('storage/photos/'.$item->bukti_foto)}}" style="max-height: 100px" alt="">
                    </td>
                    <td><a type="button" class="btn btn-info" href="/cetakbuktipemusnahan/{{$item['id']}}">Cetak</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>    
@endsection