@extends('template')
@section('content')
    <div class="">
        <form action="/pemusnahan/store"  enctype="multipart/form-data" method="POST">
            @csrf   
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label>APD</label>
                    <select class="form-control select2" id="apd" name="apd" style="width: 100%;">
                      <option selected="selected">Pilih Apd</option>
                      @foreach ($apd as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>                          
                      @endforeach
                    </select>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jumlah APD</label>
                  <input type="text" class="form-control" id="jumlah_apd" name="jumlah_apd">
                </div>
                <!-- /.form-group -->
              </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label>Bukti Foto</label><br>
                      <input type="file" id="bukti" name="bukti" accept="image/png, image/jpeg">
                    </div>
                    <!-- /.form-group -->
                  </div>
            </div>
              <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>    
@endsection