@extends('template')
@section('content')
    @if (Auth::user()->role == 'admin')
      @if ($data != null)    
        @foreach ($data as $item)    
            @if ($item['stock'] < $item['stock_min'])
                <div class="container-fluid" style="background-color: red; color:white;">
                    <h5>PERINGATAN</h5>
                    <p> Stock dari {{$item['apd']}} kurang dari stock minimal, stock tersedia : {{$item['stock']}}. Segera Lakukan <a href="/stockapd" style="color:white;text-decoration: underline;">Pemesanan Ulang</a>  </p>
                </div>
            @endif
        @endforeach
      @endif
    @endif
    <h2>Selamat Datang {{ Auth::user()->name }} sebagai {{ Auth::user()->role }}</h2>
    <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Visi
              </button>
            </h2>
          </div>
      
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                @php
                    $visi = App\Models\Visimisi::where('type','visi')->first();   
                @endphp
                @if($visi != null)
                    {{$visi->isi}}
                @else
                    Visi belum diisi, <a href="#">silahkan diisi terlebih dahulu</a>
                @endif
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Misi
              </button>
            </h2>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                @php
                $misi = App\Models\Visimisi::where('type','misi')->first();   
            @endphp
            @if($misi != null)
                {{$misi->isi}}
            @else
                Visi belum diisi, <a href="#">silahkan diisi terlebih dahulu</a>
            @endif
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingThree">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                SOP
              </button>
            </h2>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                @php
                    $sop = App\Models\Sop::get();
                @endphp
                @if($sop != null)
                    <ol>
                        @foreach ($sop as $item)
                            <li>
                                {{$item->isi_sop}}
                            </li>    
                        @endforeach
                    </ol>
                @else
                    SOP belum diisi, <a href="#">silahkan diisi terlebih dahulu</a>
                @endif
            </div>
          </div>
        </div>
      </div>
@endsection