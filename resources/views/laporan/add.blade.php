@extends('template')
@section('title')
    Buat Laporan Pelanggaran
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/laporan/add" method="POST">
          @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label>Pilih Terlapor</label>
                    <select class="form-control select2" id="terlpaor" name="terlapor" style="width: 100%;">
                        <option selected="selected">Pilih Terlapor</option>
                        @foreach ($user as $item=>$users)
                        <option value="{{$users->id}}">{{$users->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Pelanggaran yang dilakukan</label>
                    <input class="form-control" type="text" id="pelanggaran" name="pelanggaran">
                </div>
              </div>
              <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Tindakan yang diambil</label>
                <select class="form-control select2" id="tindakan" name="tindakan" style="width: 100%;">
                    <option value="Peringatan Ringan">Peringatan Ringan</option>
                    <option value="Pemberian SP 1">Pemberian SP 1</option>
                    <option value="Pemberian SP 2">Pemberian SP 2</option>
                    <option value="Pemberian SP 3">Pemberian SP 3</option>
                </select>
            </div>
                <!-- /.form-group -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
          <!-- /.col -->
        </div>
@endsection