@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <a type="button" class="btn btn-danger" href="/export-excels-laporan">Cetak Excel</a>
            <br>
            <br>
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>Pelapor</th>
                    <th>Terlapor</th>
                    <th>Pelanggaran</th>
                    <th>Tindakan</th>
                    <th>Cetak Bukti</th>
                </thead>
                    @forelse ($data as $key => $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item['pelapor']}}</td>
                        <td>{{$item['terlapor']}}</td>
                        <td>{{$item['pelanggaran']}}</td>
                        <td>{{$item['tindakan']}}</td>
                        <td><a type="button" class="btn btn-info" href="/cetakbuktilaporan/{{$item['id']}}">Cetak</a></td>
                    </tbody>    
                    @empty
                        
                    @endforelse             
            </table>
        </div>
    </div>    
@endsection