@extends('template')
@section('title')
    Buat Pengajuan APD
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/pengajuan/store" method="POST">
          @csrf
          @for ($i = 1  ; $i < $loop1; $i++)    
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label>APD</label>
                  <select class="form-control select2" id="apd" name="apd[]" style="width: 100%;">
                    <option selected="selected">Pilih Apd</option>
                    @foreach ($apd as $item)
                      <option value="{{$item->id}}">{{$item->nama}}</option>                          
                    @endforeach
                  </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Jumlah APD</label>
                <input type="text" class="form-control" id="jumlah_apd" name="jumlah_apd[]" 
                
                @if (
                  Auth::user()->role=='pekerja'
                )
                value='1'
                readonly
                @endif
                >
              </div>
              <!-- /.form-group -->
            </div>
            <div id="dynamic-input">

            </div>
          </div>
          @endfor
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
          <!-- /.col -->
        </div>
@endsection