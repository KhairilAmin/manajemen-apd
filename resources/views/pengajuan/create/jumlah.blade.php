@extends('template')
@section('title')
    Buat Pengajuan APD
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/create-pengajuan" method="POST">
          @csrf
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label>Jumlah jenis APD yang ingin di ajukan :</label>
                    <select class="form-control select2" id="jumlah" name="jumlah" style="width: 100%;">
                      {{-- <option selected="selected">Pilih Apd</option> --}}
                      <option value="1">1</option>                          
                      <option value="2">2</option>                          
                      <option value="3">3</option>                          
                      <option value="4">4</option>                          
                      <option value="5">5</option>                          
                    </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
          <!-- /.col -->
        </div>        
@endsection