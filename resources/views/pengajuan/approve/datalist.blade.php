@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>Nomor Pengajuan</th>
                    <th>APD</th>
                    <th>Pengaju</th>
                    <th>Approved by Atasan</th>
                    <th>Approved by Admin</th>
                    <th>Cetak</th>
                </thead>
                    @forelse ($data as $key => $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item['nomor_pengajuan']}}</td>
                        <td>

                            @php
                                $detail = App\Models\PengajuanDetail::where('id_pengajuan',$item['id'])->get();
                                $data1=[];
                                foreach($detail as $key => $item1) {
                                    $apd = App\Models\Apd::where('id', $item1->id_apd)->first();
                                    $data1[$key]['apd'] = $apd->nama;
                                    $data1[$key]['jumlah'] = $item1->jumlah_apd;
                                }
                                
                            @endphp
                            
                            @foreach ( $data1 as $item1)
                                {{$item1['apd']}} , Jumlah : {{$item1['jumlah']}} <br>
                            @endforeach

                        </td>
                        <td>{{$item['pengaju']}}</td>
                        <td>
                            @if ($auth->role == 'atasan')
                                @if ($item['approved'] == 0)
                                    <a href="/approveatasan/{{$item['id']}}" class="btn btn-success">Approve</a>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-reject-{{$item['id']}}">
                                        Reject
                                    </button>
                                    <div class="modal fade" id="modal-reject-{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="/rejectatasan/{{$item['id']}}" method="POST">
                                                    @csrf
                                                    <div class="mb-2">
                                                        <div class="form-group">
                                                          <label>Alasan ditolak</label>
                                                          <input type="text" class="form-control" id="alasan" name="alasan">
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    {{-- <a href="/rejectatasan/{{$item['id']}}" class="btn btn-danger">Reject</a> --}}
                                @elseif ($item['approved'] == 1)
                                    <p class="text-success">Approved</p>
                                @elseif ($item['approved'] == 10)
                                    <p class="text-danger">Rejected</p>
                                @endif
                            @elseif($auth->role == 'admin')
                                @if ($item['approved'] == 0)
                                    <p class="text-warning">Dalam Proses Pengajuan</p>
                                @elseif ($item['approved'] == 1)
                                    <p class="text-success">Approved</p>
                                @elseif ($item['approved'] == 10)
                                    <p class="text-danger">Rejected</p>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if ($auth->role == 'admin')
                                @if ($item['approved'] == 0 )
                                    <p class="text-danger">Belum diaprove atasan</p>
                                @elseif( $item['approved'] ==1 )
                                    @if ($item['approved_by_admin'] == 0)
                                        <a href="/approveadmin/{{$item['id']}}" class="btn btn-success">Approve</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{$item['id']}}">
                                            Reject
                                        </button>
                                        <div class="modal fade" id="modal-delete-{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="/rejectadmin/{{$item['id']}}" method="POST">
                                                        @csrf
                                                        <div class="mb-3">
                                                            <div class="form-group">
                                                              <label>Alasan ditolak</label>
                                                              <input type="text" class="form-control" id="alasan" name="alasan">
                                                            </div>
                                                            <!-- /.form-group -->
                                                        </div>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </form>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                    @elseif ($item['approved_by_admin'] == 1)
                                        <p class="text-success">Approved</p>
                                    @elseif ($item['approved_by_admin'] == 10)
                                        <p class="text-danger">Rejected</p>
                                    @endif
                                @elseif( $item['approved'] ==10 )
                                    <p class="text-danger">Ditolak oleh atasan</p>
                                @endif
                            @elseif($auth->role == 'atasan')
                                @if ($item['approved_by_admin'] == 0)
                                    <p class="text-warning">Dalam Proses Pengajuan</p>
                                @elseif ($item['approved_by_admin'] == 1)
                                    <p class="text-success">Approved</p>
                                @elseif ($item['approved_by_admin'] == 10)
                                    <p class="text-danger">Rejected</p>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if ($item['approved_by_admin'] == 0)
                                <p class="text-warning">Belum di approve admin</p>
                            @elseif ($item['approved_by_admin'] == 1)
                                <a type="button" class="btn btn-info" href="/cetakbukti/{{$item['id']}}">Cetak</a>
                            @elseif ($item['approved_by_admin'] == 10)
                                <p class="text-danger">Rejected by admin</p>
                            @endif
                        </td>
                    </tbody>    
                    @empty
                        
                    @endforelse             
            </table>
        </div>
    </div>    
@endsection