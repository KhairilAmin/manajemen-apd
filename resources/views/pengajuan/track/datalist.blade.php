@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>Nomer Pegajuan</th>
                    <th>APD</th>
                    <th>Pengaju</th>
                    <th>Approved by Atasan</th>
                    <th>Approved by Admin</th>
                </thead>
                    @forelse ($data as $key => $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item['nomor_pengajuan']}}</td>
                        <td>

                            @php
                                $detail = App\Models\PengajuanDetail::where('id_pengajuan',$item['id'])->get();
                                $data1=[];
                                foreach($detail as $key => $item1) {
                                    $apd = App\Models\Apd::where('id', $item1->id_apd)->first();
                                    $data1[$key]['apd'] = $apd->nama;
                                    $data1[$key]['jumlah'] = $item1->jumlah_apd;
                                }
                                
                            @endphp
                            
                            @foreach ( $data1 as $item1)
                                {{$item1['apd']}} , Jumlah : {{$item1['jumlah']}} <br>
                            @endforeach

                        </td>
                        <td>{{$item['pengaju']}}</td>
                        <td>
                                @if ($item['approved'] == 0)
                                    <p class="text-warning">Dalam Proses Pengajuan</p>
                                @elseif ($item['approved'] == 1)
                                    <p class="text-success">Approved</p>
                                @elseif ($item['approved'] == 10)
                                    <div class="row">
                                        <p class="text-danger my-auto">Rejected</p>
                                        <button type="button" class="btn btn-danger ml-2" data-toggle="modal" data-target="#modal-alasan-{{$item['id']}}">
                                            Cek
                                        </button>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" id="modal-alasan-{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Kenapa pengajuan ditolak?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{$item['alasan']}}</p>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                @endif
                        </td>
                        <td>
                            @if ($item['approved']==10)
                                <p class="text-danger">Rejected by atasan</p>
                            @elseif($item['approved']==1||$item['approved']==0)
                                @if ($item['approved_by_admin'] == 0)
                                    <p class="text-warning">Dalam Proses Pengajuan</p>
                                @elseif ($item['approved_by_admin'] == 1)
                                    <p class="text-success">Approved</p>
                                @elseif ($item['approved_by_admin'] == 10)
                                    <p class="text-danger">Rejected</p>
                                @endif
                            @endif
                        </td>
                    </tbody>    
                    @empty
                        
                    @endforelse             
            </table>
        </div>
    </div>    
@endsection