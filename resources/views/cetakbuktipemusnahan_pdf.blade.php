<!DOCTYPE html>
<html lang="en">
<style>
    .header h1{
        text-align: center;
    }
</style>
<body>
    <div class="header">
        <h1>Bukti Pemusnahan APD</h1>
    </div>
    <hr>
    @forelse ($data as $key => $item)    
    <div>
        <p>
            Nama Pemusnah &emsp;&emsp;&emsp;: {{$item['id_pemusnah']}}
		<br>
			Jenis APD&emsp;&ensp;: {{$item['id_apd']}}
		<br>
			Jumlah APD&emsp;&ensp;: {{$item['jumlah_apd']}}
		<br>
            Bukti Foto&emsp;&ensp;:<br>
            <img src="{{ storage_path('app/public/photos/'.$item['bukti_foto']) }}" style="width: 200px; height: 200px">
        <br>
    </div>
    @empty
        
    @endforelse
</body>
</html>