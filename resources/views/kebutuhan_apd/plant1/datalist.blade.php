@extends('template')
@section('title')
    Plant 1
@endsection
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            @if (Auth::user()->role == 'admin')
            <a href="/addplant1" class="btn btn-warning" style="margin-bottom: 10px">Tambah Data</a>
            @endif
            <table class="table table-bordered user_datatable">
                <thead>
                    <tr align="center">
                        <th width="100px">NO</th>
                        <th>Job</th>
                        <th>APD</th>
                        @if (Auth::user()->role == 'admin')
                        <th width="300px">Action</th>
                        @endif
                    </tr>
                </thead>
                @forelse ($plant1 as $key => $item)
                    <tbody>
                        <tr align="center">
                            <td>{{$key+1}}</td>
                            <td>{{$item->job}}</td>
                            <td>{{$item->apd}}</td>
                            @if (Auth::user()->role == 'admin')
                            <td><a type="button" href="/editplant1/{{$item->id}}" class="btn btn-warning m-1">Edit</a><a type="button" href="/plant1/delete/{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                            @endif
                        </tr>
                    </tbody>
                @empty
                    
                @endforelse           
            </table>
        </div>
    </div>    
@endsection