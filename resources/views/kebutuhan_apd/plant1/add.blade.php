@extends('template')
@section('title')
    Add Plant1
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/plant1/add" method="POST">
          @csrf
            <div class="col-12">
                <div class="from-group">
                    <label for="plant1">Job</label>
                    <input type="text" class="form-control" name="job">
                    <label for="plant1">apd</label>
                    <input type="text" class="form-control" name="apd">
                </div>
            </div>
            <button type="submit" class="btn btn-primary m-2">Submit</button>
        </form>
          <!-- /.col -->
        </div>

@endsection