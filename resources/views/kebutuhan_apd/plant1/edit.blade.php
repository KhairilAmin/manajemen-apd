@extends('template')
@section('title')
    Edit Plant1
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/plant1/edit/{{$plant1->id}}" method="POST">
          @csrf
            <div class="col-12">
                <div class="from-group">
                    <label for="plant1">Job</label>
                    <textarea class="form-control" type="text" name="job" id="name">{{$plant1->job}}</textarea>
                    <label for="plant1">APD</label>
                    <textarea class="form-control" type="text" name="apd" id="name">{{$plant1->apd}}</textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary m-2">Submit</button>
        </form>
          <!-- /.col -->
        </div>

@endsection