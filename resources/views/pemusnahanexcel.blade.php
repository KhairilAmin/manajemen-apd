<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>No. </th>
                <th>Pemusnah</th>
                <th>APD</th>
                <th>Jumlah</th>
                <th>Bukti</th>
            </tr>
        </thead>
        @php
            $i = 1;
        @endphp
        <tbody>
            @foreach ($datas as $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data->pemusnah->name}}</td>
                    <td>{{$data->apd->nama}}</td>
                    <td>{{$data->jumlah_apd}}</td>
                    <td>
                        <img src="{{ storage_path('app/public/photos/'.$data->bukti_foto) }}" width="150" style="width: 200px; height: 200px">
                    </td>
                    
                </tr>        
            @endforeach
        </tbody>
    </table>
</body>
</html>