<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>No. </th>
                <th>Pelapor</th>
                <th>Terlapor</th>
                <th>Pelanggaran</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        @php
            $i = 1;
        @endphp
        <tbody>
            @foreach ($datas as $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data->pelapor->name}}</td>
                    <td>{{$data->terlapor->name}}</td>
                    <td>{{$data->pelanggaran}}</td>
                    <td>{{$data->tindakan}}</td>
                </tr>        
            @endforeach
        </tbody>
    </table>
</body>
</html>