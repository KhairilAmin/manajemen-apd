@extends('template')
@section('title')
    Edit Visi
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/edit/{{$visi->type}}" method="POST">
          @csrf
            <div class="col-12">
                <div class="from-group">
                    <label for="visi">Visi</label>
                    <textarea class="form-control" type="text" name="visi" id="name">{{$visi->isi}}</textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary m-2">Submit</button>
        </form>
          <!-- /.col -->
        </div>

@endsection