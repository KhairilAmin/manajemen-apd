@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <a href="/addsop" type="button" class="btn btn-success mb-2">Tambah SOP</a>
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>Isi SOP</th>
                    <th>Action</th>
                </thead>
                    @php
                        $key = 0;
                    @endphp
                    @forelse ($sop as $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item->isi_sop}}</td>
                        <td><a type="button" href="/editsop/{{$item->id}}" class="btn btn-warning m-1">Edit</a><a type="button" href="/sop/delete/{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                    </tbody> 
                    @php
                        $key++;
                    @endphp   
                    @empty
                        
                    @endforelse             
            </table>
        </div>
    </div>    
@endsection