@extends('template')
@section('title')
    Add SOP
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/sop/add" method="POST">
          @csrf
            <div class="col-12">
                <div class="from-group">
                    <label for="sop">SOP</label>
                    <textarea class="form-control" type="text" name="sop" id="name"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary m-2">Submit</button>
        </form>
          <!-- /.col -->
        </div>

@endsection