@extends('template')
@section('title')
    Edit Misi
@endsection
@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="card-body">
        <form action="/edit/{{$misi->type}}" method="POST">
          @csrf
            <div class="col-12">
                <div class="from-group">
                    <label for="visi">Misi</label>
                    <textarea class="form-control" type="text" name="misi" id="name">{{$misi->isi}}</textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary m-2">Submit</button>
        </form>
          <!-- /.col -->
        </div>

@endsection