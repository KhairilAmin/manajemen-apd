@extends('template')
@section('title')
    Stock APD
@endsection
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Lihat Riwayat APD
            </button>
            <br>
            <br>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Riwayat APD</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <a type="button" class="btn btn-danger" href="/export-excels-riwayat">Cetak Excel</a>
                      @foreach ($riwayat as $item)
                        @php
                            $apd1 = App\Models\Apd::where('id',$item->id_apd)->first();
                        @endphp
                          <p style="color : {{ $item->action === "Penambahan" ? "green" : "red"}}">
                            <br>
                            {{$item->created_at}}<br>
                            {{$item->action}} APD {{$apd1->nama}} sejumlah {{$item->jumlah_apd}} pcs
                          </p>
                      @endforeach
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>APD</th>
                    <th>Stock Tersedia</th>
                    <th>Stock Minimal</th> 
                    <th>Tambah Stock</th> 
                </thead>
                @forelse ($data as $key => $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item['apd']}}</td>
                        <td>{{$item['stock']}}</td>
                        <td>{{$item['stock_min']}}</td>
                        <td><a class="btn btn-success" href="/pesanapd/{{$item['id_apd']}}" role="button">Pesan APD</a></td>
                    </tbody>
                @empty
                    
                @endforelse           
            </table>
        </div>
    </div>    
@endsection