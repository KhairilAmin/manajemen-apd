@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <form action="/actionaddstock/{{$apd->id}}" method="post">
                @csrf
                <label for="apd">APD</label>
                <input type="text" class="form-control" name="apd" value="{{$apd->nama}}" readonly>
                <label for="jumlah_apd">Jumlah Pesanan</label>
                <input type="text" class="form-control" name="jumlah_apd">
                <button type="submit" class="btn btn-primary" style="margin-top: 10px">Submit</button>
            </form>
        </div>
    </div>    
@endsection