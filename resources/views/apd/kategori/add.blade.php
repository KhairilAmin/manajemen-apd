@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <form action="/actionaddkategori" method="post">
                @csrf
                <label for="kategori">Kategori</label>
                <input type="text" class="form-control" name="kategori">
                <button type="submit" class="btn btn-primary" style="margin-top: 10px">Submit</button>
            </form>
        </div>
    </div>    
@endsection