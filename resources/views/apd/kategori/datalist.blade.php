@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <a href="/addkategori" class="btn btn-warning" style="margin-bottom: 10px">Tambah Kategori</a>
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>Kategori</th>
                </thead>
                @forelse ($kategori as $key => $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item->kategori}}</td>
                    </tbody>
                @empty
                    
                @endforelse           
            </table>
        </div>
    </div>    
@endsection