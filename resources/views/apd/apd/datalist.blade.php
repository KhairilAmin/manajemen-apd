@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <a href="/addapd" class="btn btn-warning" style="margin-bottom: 10px">Tambah Apd</a>
            <table class="table table-bordered user_datatable">
                <thead>
                    <th>NO</th>
                    <th>APD</th>
                    <th>Kategori</th>
                </thead>
                @forelse ($data as $key => $item)
                    <tbody>
                        <td>{{$key+1}}</td>
                        <td>{{$item['apd']}}</td>
                        <td>{{$item['kategori']}}</td>
                    </tbody>
                @empty
                    
                @endforelse           
            </table>
        </div>
    </div>    
@endsection