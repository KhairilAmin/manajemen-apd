@extends('template')
@section('content')
    <div class="row">
        <div class="col-12 table-responsive">
            <form action="/actionaddapd" method="post">
                @csrf
                <div class="form-group">
                    <label>Kategori Apd</label>
                    <select class="form-control select2" id="kategori" name="kategori" style="width: 100%;">
                        <option selected="selected">Pilih Kategori Apd</option>
                        @foreach ($kategori as $item=>$kategori)
                        <option value="{{$kategori->id}}">{{$kategori->kategori}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Apd</label>
                    <input type="text" name="apd" class="form-control">
                </div>
                <div class="form-group">
                    <label>Stock Minimal</label>
                    <input type="text" name="stock_min" class="form-control">
                </div>
                <div class="form-group">
                    <label>Mempunyai Expired?</label>
                    <select class="form-control select2" id="has_exp" name="has_exp" style="width: 100%;">
                        <option value="0">Tidak</option>
                        <option value="1">Iya</option>
                    </select>
                </div>
                <div class="form-group" id="jangkawaktu" style="display:none">
                    <label>Berapa hari?</label>
                    <input type="text" name="jangkawaktu" class="form-control">
                </div>                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        $("document").ready(function () {
          $('select[name="has_exp"]').on('change', function () {
            var exp = $(this).val();
            if (exp == 1){
                $( "#jangkawaktu" ).css( "display", "block" );
            } else if(exp == 0){
                $( "#jangkawaktu" ).css( "display", "none" );
            }

            
          })
        });
    </script>    
@endsection