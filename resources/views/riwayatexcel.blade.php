<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>No. </th>
                <th>Apd</th>
                <th>Action</th>
                <th>Jumlah</th>
                <th>Tanggal</th>
            </tr>
        </thead>
        @php
            $i = 1;
        @endphp
        <tbody>
            @foreach ($datas as $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data->apd->nama}}</td>
                    <td>{{$data->action}}</td>
                    <td>{{$data->jumlah_apd}}</td>
                    <td>{{$data->created_at}}</td>
                    
                </tr>        
            @endforeach
        </tbody>
    </table>
</body>
</html>