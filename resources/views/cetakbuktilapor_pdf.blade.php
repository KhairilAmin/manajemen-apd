<!DOCTYPE html>
<html lang="en">
<style>
    .header h1{
        text-align: center;
    }
</style>
<body>
    <div class="header">
        <h1>Bukti Pelaporan</h1>
    </div>
    <hr>
    @forelse ($data as $key => $item)    
    <div>
        <p>
            Nama Pelapor &emsp;&emsp;&emsp;: {{$item['pelapor']}}
        <br>     
            Nama Terlapor&emsp;&ensp;: {{$item['terlapor']}}
        <br>
            Pelanggaran&emsp;&ensp;: {{$item['pelanggaran']}}
		<br>
            Tindakan&emsp;&emsp;&ensp;: {{$item['tindakan']}}
		<br>
    </div>
    @empty
        
    @endforelse
</body>
</html>