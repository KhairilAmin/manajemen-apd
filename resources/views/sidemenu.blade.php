        <!-- Main Sidebar Container -->

        {{-- #000035 --}}
        {{-- #084E11 --}}
        <aside class="main-sidebar sidebar-dark-light elevation-4" style="color: white; background-color:#000035;">
            <!-- Brand Logo -->
            <a href="{{asset('admin/index3.html')}}" class="brand-link">
            <img src="{{asset('admin/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">SM-APD</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                {{-- <div class="image">
                    <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                </div> --}}
                <div class="info">
                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                </div>
                </div>
        
                <!-- SidebarSearch Form -->
                <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input style="background-color: #000035;color: white" class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                    <button class="btn btn-sidebar " style="background-color: #000035;color: white">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                    </div>
                </div>
                </div>
    
            <!-- Sidebar Menu -->
            
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/home" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Dashboard
                    </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Pengajuan APD
                        <i class="fas fa-angle-left right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="pengajuan/jumlah" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Buat pengajuan APD</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/trackpengajuan" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Track Proses Pengajuan</p>
                        </a>
                    </li>
                    @if (Auth::user()->role == 'atasan')
                    <li class="nav-item">
                        <a href="/approve" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Approve Pengajuan</p>
                        @php
                            $pengajuan = App\Models\Pengajuan::where('approved',0)->count();
                        @endphp
                        @if ($pengajuan > 0)
                            <span class="badge badge-pill badge-danger">{{$pengajuan}}</span>
                        @endif
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'admin')    
                    <li class="nav-item">
                        <a href="/approve" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Approve Pengajuan by Admin</p>
                        @php
                            $pengajuan = App\Models\Pengajuan::where('approved',1)->where('approved_by_admin',0)->count();
                        @endphp
                        @if ($pengajuan > 0)
                            <span class="badge badge-pill badge-danger">{{$pengajuan}}</span>
                        @endif
                        </a>
                    </li>
                    @endif
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Pemusnahan APD
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/pemusnahanapd" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Pemusnahan</p>
                        </a>
                    </li>
                    </ul>
                </li>
                @if (Auth::user()->role=='admin')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p>
                        Stock Gudang APD
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/kategoriapd" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori APD</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexapd" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>APD</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/stockapd" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Stock APD</p>
                        </a>
                    </li>
                    </ul>
                </li>
                @if (Auth::user()->role == 'atasan'|| Auth::user()->role == 'admin')
                        
                @endif
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-file"></i>
                    <p>
                        Edit SOP & Visi Misi
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/indexsop" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>SOP</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexvisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Visi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Misi</p>
                        </a>
                    </li>
                    </ul>
                </li>
                @endif
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-edit"></i>
                    <p>
                        Laporan Pelanggaran
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    @if (Auth::user()->role == 'atasan'|| Auth::user()->role == 'admin')
                    
                    <li class="nav-item">
                        <a href="/indexlaporan" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Laporan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/addlaporan" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Buat Laporan</p>
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a href="/pelanggaransaya" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Pelanggaran Saya</p>
                        </a>
                    </li>
                    </ul>
                </li>
                
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-file"></i>
                    <p>
                        Kebutuhan APD
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/indexplant1" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Plant 1</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexvisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Plant 2</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Formulasi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Warehouse</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Teknik</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Utility</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Laboratorium</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>SHE</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/indexmisi" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Office</p>
                        </a>
                    </li>
                    </ul>
                </li>        

                <li class="nav-item ">
                    </a>
                    <a class="nav-link btn-danger" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        <i class="nav-icon fa fa-sign-out"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>